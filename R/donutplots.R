#' Create a smooth donutplot
#'
#' Build a donut plot which is really not recommended to do since it's pretty
#' much despised by all people who know anything about VizTech. But, anyway. I
#' like it so here it is.
#'
#' @param data a data frame compatible object which has columns category and
#' count present
#'
#' @return a ggplot of a donut plot
#' @export
#'
#' @examples
#' tmpdf <- data.frame(
#'   category = c("Male", "Female", "Bovine", "Arachnea", "Vertebrae"),
#'   count = c(40, 60, 30, 80, 20)
#' )
#' plotdonut(tmpdf)
plotdonut <- function(data) {
  # Compute percentages
  data <- data %>% dplyr::mutate(
    fraction = count / sum(count), ymax = cumsum(fraction),
    ymin = c(0, utils::head(ymax, n = -1)),
    labelPosition = (ymax + ymin) / 2,
    label = paste0(category, "\n value: ", count)
  )
  # Make the plot
  ymax <- ymin <- xmin <- xmax <- fraction <- category <- label <- labelPosition <- NULL
  ggplot(data, aes(ymax = ymax, ymin = ymin, xmax = 4, xmin = 3, fill = category)) +
    ggplot2::geom_rect() +
    ggplot2::geom_text(x = 2, aes(y = labelPosition, label = label, color = category), size = 6) + # x here controls label position (inner / outer)
    ggplot2::scale_fill_brewer(palette = 3) +
    ggplot2::scale_color_brewer(palette = 3) +
    ggplot2::coord_polar(theta = "y") +
    ggplot2::xlim(c(-1, 4)) +
    ggplot2::theme_void() +
    ggplot2::theme(legend.position = "none")
}
